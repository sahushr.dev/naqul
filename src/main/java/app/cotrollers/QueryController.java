package app.cotrollers;

import app.base.model.TaggedWord;
import app.base.model.request.Request;
import app.services.QueryService;
import app.services.TaggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */

@RestController
public class QueryController {

    @Autowired
    QueryService queryService;

    @Autowired
    TaggingService taggingService;

    @RequestMapping("/query")
    public Set<String> query(@RequestBody Request request) {
        System.out.println("Request query : " + request);
        Set<String> parseQuery = queryService.parseQuery(request.getNql());
        return parseQuery;
    }

    @RequestMapping("/tag/words")
    public List<TaggedWord> taggedWords(@RequestParam(value = "text") String nql) {
        System.out.println("Request query : " + nql);
        return taggingService.getSqlTaggedWords(nql);

    }
}
