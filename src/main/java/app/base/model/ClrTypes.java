package app.base.model;

import java.util.*;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */
public enum ClrTypes {
    STRING("CHAR", "VARCHAR", "BINARY", "VARBINARY", "BLOB", "TEXT", "ENUM", "SET"),
    DOUBLE("FLOAT", "DOUBLE", "DECIMAL", "NUMERIC"),
    INTEGER("INTEGER", "INT", "SMALLINT", "TINYINT", "MEDIUMINT", "BIGINT"),
    OTHERS;


    public List<String> associatedSqlTypes;

    private static Map<String, ClrTypes> lookup = new HashMap<>();

    ClrTypes(String... sqlType) {
        associatedSqlTypes = Arrays.asList(sqlType);
    }

    static {
        Arrays.stream(ClrTypes.values()).forEach(clrTypes -> {
            if (clrTypes.associatedSqlTypes != null)
                clrTypes.associatedSqlTypes.forEach(s -> lookup.put(s, clrTypes));
        });
    }

    public static ClrTypes getClrType(String sqlType) {
        String[] split = sqlType.split("\\(");
        String s = split[0].toUpperCase();
        if (lookup.keySet().contains(s)) return lookup.get(s);
        return OTHERS;
    }

    public static List<ClrTypes> getType(String word) {
        List<ClrTypes> clrTypes = new ArrayList<>();

        word = word.replace(" ", "");
        try {
            long v = Long.parseLong(word);
            clrTypes.add(INTEGER);


            double v1 = Double.parseDouble(word);
            clrTypes.add(DOUBLE);

        } catch (NumberFormatException ex) {
            clrTypes.add(STRING);
        }

        return clrTypes;

    }

}
