package app.base.model;

public interface TagDetail {
    String getTag();
    String getName();
}
