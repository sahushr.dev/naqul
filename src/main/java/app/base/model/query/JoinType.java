package app.base.model.query;

public enum JoinType {
    JOIN,
    INNER_JOIN,
    OUTER_JOIN,
    LEFT_INNER_JOIN,
    RIGHT_INNER_JOIN,
    LEFT_OUTER_JOIN,
    RIGHT_OUTER_JOIN,
}
