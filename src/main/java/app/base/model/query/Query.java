package app.base.model.query;

import app.base.model.sql.SqlFunctions;
import app.base.model.sql.SqlKeywords;
import app.base.model.sql.SqlOperators;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */
@Data
@EqualsAndHashCode
public class Query {
    private Set<String> selectList = new HashSet<>();
    private String from;
    private Set<String> whereList = new HashSet<>();
    private Set<String> groupByList = new HashSet<>();
    private Set<String> havingList = new HashSet<>();
    private List<Join> joinList = new ArrayList<>();
    private Set<String> orderList = new HashSet<>();

//    public void addToSelectList(String columnName) {
//        selectList.add(columnName);
//    }
//
//    public void addToWhereList(String expression) {
//        whereList.add(expression);
//    }
//
//    public void addToGroupByList(String columnName) {
//        groupByList.add(columnName);
//    }
//
//    public void addToHavingList(String expression) {
//        havingList.add(expression);
//    }
//
//    public void addToJoinList(String table, Expression expression, JoinType joinType) {
//        joinList.add(new Join(joinType, table, expression));
//    }
//
//    public void addToJoinList(String table, Expression expression) {
//        addToJoinList(table, expression, JoinType.JOIN);
//    }

    public String selectAsString() {
        String select = SqlKeywords.SELECT + " ";
        if (selectList != null && !selectList.isEmpty()) {
            return select + String.join(", ", selectList) + " ";
        } else return select + " * ";
    }

    public String groupByAsString() {
        String groupBy = SqlKeywords.GROUP + " " + SqlKeywords.BY + " ";
        if (groupByList != null && !groupByList.isEmpty())
            return groupBy + String.join(", ", groupByList) + " ";
        return null;

    }

    public String fromAsString() {
        String from = SqlKeywords.FROM + " ";
        if (this.from != null && !this.from.isEmpty()) return from + this.from + " ";
        return null;
    }

    public String havingListAsString() {
        String having = SqlKeywords.HAVING + " ";
        if (havingList != null && !havingList.isEmpty())
            return having + String.join(", ", havingList);
        return null;
    }

    public String joinListAsString() {
        if (joinList != null && !joinList.isEmpty()) {
            return joinList.stream().map(join -> " " + join.joinType + " " + join.table + " " + SqlKeywords.ON + " " + join.on + " ").reduce(String::concat).get();
        }
        return null;
    }

    public String whereAsString() {
        if (whereList != null && !whereList.isEmpty())
            return SqlKeywords.WHERE + " " + String.join(" AND ", whereList);
        return null;
    }

    public String orderAsString() {
        if(orderList != null && !orderList.isEmpty())
            return SqlKeywords.ORDER + " " + SqlKeywords.BY + " " + String.join(",",orderList);
        return null;
    }


    public String selectAsString(Set<String> sqlKeywords) {

        if (sqlKeywords.contains(SqlOperators.ALL.getAbbreviation()))
            return SqlKeywords.SELECT + " * ";

        if (sqlKeywords.contains(SqlFunctions.COUNT.getAbbreviation())) {

            String list = SqlFunctions.COUNT + "(*) ";

            if (groupByList != null && !groupByList.isEmpty()) {
                list = list + " , " + String.join(" , ", groupByList) + " ";
            }
            return SqlKeywords.SELECT + " " + list;
        }

        return selectAsString();
    }

    public String whereAsString(Set<String> sqlOperators) {
        sqlOperators.remove(SqlOperators.ALL.getAbbreviation());
        final String s;
        if (sqlOperators.size() > 0)
            s = sqlOperators.toArray()[0].toString();
        else s = "=";
        if (whereList != null && !whereList.isEmpty()) {
            Set<String> collect = whereList.stream().map(s1 -> s1.replace("=", s)).collect(Collectors.toSet());
            return SqlKeywords.WHERE + " " + String.join(" AND ", collect);
        }
        return null;
    }
}


