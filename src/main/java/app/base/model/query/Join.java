package app.base.model.query;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Join {
    JoinType joinType;
    String table;
    String on;

    public Join(String table, String on) {
        this.joinType = JoinType.JOIN;
        this.table = table;
        this.on = on;
    }
}
