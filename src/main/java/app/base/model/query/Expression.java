package app.base.model.query;

import app.base.model.sql.SqlOperators;

/**
 * @author shraddha.sahu on 28/09/18
 * @project NaQuL
 */
public class Expression {
    SqlOperators operator;
    String left;
    String right;
}
