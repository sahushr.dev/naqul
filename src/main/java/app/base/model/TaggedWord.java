package app.base.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaggedWord {
    String word;
    List<TagDetail> Value = new ArrayList<>();
}
