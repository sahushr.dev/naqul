package app.base.model.sql;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */
public interface SqlKnownWords {
    String getAbbreviation();
}
