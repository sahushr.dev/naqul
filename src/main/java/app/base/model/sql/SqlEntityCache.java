package app.base.model.sql;

import app.services.PythonService;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */

@Component
public class SqlEntityCache {
    private static final Map<String, Set<SqlEntity>> lookup = new HashMap<>();


    public static void put(List<SqlEntity> words) {


        words.forEach(sqlKnownWord -> {
            String key = sqlKnownWord.value.toUpperCase();
            if(!lookup.containsKey(key))
                lookup.put(key,new HashSet<>());
            lookup.get(key).add(sqlKnownWord);

            PythonService.getWordSynonyms(sqlKnownWord.value).forEach(s -> {
                String key1 = s.toUpperCase();
                if(!lookup.containsKey(s))
                    lookup.put(key1,new HashSet<>());
                lookup.get(key1).add(sqlKnownWord);
                lookup.get(key1).add(sqlKnownWord);
            });
        });
    }

    public static Set<SqlEntity> get(String word) {
        return lookup.getOrDefault(word,new HashSet<SqlEntity>());
    }

    public static boolean contains(String word) {
       return lookup.containsKey(word);
    }

}
