package app.base.model.sql;

import app.base.model.TagDetail;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SqlEntity implements TagDetail {
    public String value;
    public String tag;
    public SqlKnownWords name;

    public String getName(){
        return name.getAbbreviation();
    }

}
