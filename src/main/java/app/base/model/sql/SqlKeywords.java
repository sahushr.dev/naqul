package app.base.model.sql;

import app.base.model.db.Column;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */
public enum SqlKeywords implements SqlKnownWords{
    ADDEXTERNAL ( "ADDEXTERNAL"),
    PROCEDURE ( "PROCEDURE"),
    ALL ( "ALL"),
    FETCH ( "FETCH"),
    PUBLIC ( "PUBLIC"),
    ALTER ( "ALTER"),
    FILE ( "FILE"),
    RAISERROR ( "RAISERROR"),
    AND ( "AND"),
    FILLFACTOR ( "FILLFACTOR"),
    READ ( "READ"),
    ANY ( "ANY"),
    FOR ( "FOR"),
    READTEXT ( "READTEXT"),
    AS ( "AS"),
    FOREIGN ( "FOREIGN"),
    RECONFIGURE ( "RECONFIGURE"),
    ASC ( "ASC"),
    FREETEXT ( "FREETEXT"),
    REFERENCES ( "REFERENCES"),
    AUTHORIZATION ( "AUTHORIZATION"),
    FREETEXTTABLE ( "FREETEXTTABLE"),
    REPLICATION ( "REPLICATION"),
    BACKUP ( "BACKUP"),
    FROM ( "FROM"),
    RESTORE ( "RESTORE"),
    BEGIN ( "BEGIN"),
    FULL ( "FULL"),
    RESTRICT ( "RESTRICT"),
    BETWEEN ( "BETWEEN"),
    FUNCTION ( "FUNCTION"),
    RETURN ( "RETURN"),
    BREAK ( "BREAK"),
    GOTO ( "GOTO"),
    REVERT ( "REVERT"),
    BROWSE ( "BROWSE"),
    GRANT ( "GRANT"),
    REVOKE ( "REVOKE"),
    BULK ( "BULK"),
    GROUP ( "GROUP"),
    RIGHT ( "RIGHT"),
    BY ( "BY"),
    HAVING ( "HAVING"),
    ROLLBACK ( "ROLLBACK"),
    CASCADE ( "CASCADE"),
    HOLDLOCK ( "HOLDLOCK"),
    ROWCOUNT ( "ROWCOUNT"),
    CASE ( "CASE"),
    IDENTITY ( "IDENTITY"),
    ROWGUIDCOL ( "ROWGUIDCOL"),
    CHECK ( "CHECK"),
    IDENTITY_INSERT ( "IDENTITY_INSERT"),
    RULE ( "RULE"),
    CHECKPOINT ( "CHECKPOINT"),
    IDENTITYCOL ( "IDENTITYCOL"),
    SAVE ( "SAVE"),
    CLOSE ( "CLOSE"),
    IF ( "IF"),
    SCHEMA ( "SCHEMA"),
    CLUSTERED ( "CLUSTERED"),
    IN ( "IN"),
    SECURITYAUDIT ( "SECURITYAUDIT"),
    COALESCE ( "COALESCE"),
    INDEX ( "INDEX"),
    SELECT ( "SELECT"),
    COLLATE ( "COLLATE"),
    INNER ( "INNER"),
    SEMANTICKEYPHRASETABLE ( "SEMANTICKEYPHRASETABLE"),
    COLUMN ( "COLUMN"),
    INSERT ( "INSERT"),
    SEMANTICSIMILARITYDETAILSTABLE ( "SEMANTICSIMILARITYDETAILSTABLE"),
    COMMIT ( "COMMIT"),
    INTERSECT ( "INTERSECT"),
    SEMANTICSIMILARITYTABLE ( "SEMANTICSIMILARITYTABLE"),
    COMPUTE ( "COMPUTE"),
    INTO ( "INTO"),
    SESSION_USER ( "SESSION_USER"),
    CONSTRAINT ( "CONSTRAINT"),
    IS ( "IS"),
    SET ( "SET"),
    CONTAINS ( "CONTAINS"),
    JOIN ( "JOIN"),
    SETUSER ( "SETUSER"),
    CONTAINSTABLE ( "CONTAINSTABLE"),
    KEY ( "KEY"),
    SHUTDOWN ( "SHUTDOWN"),
    CONTINUE ( "CONTINUE"),
    KILL ( "KILL"),
    SOME ( "SOME"),
    CONVERT ( "CONVERT"),
    LEFT ( "LEFT"),
    STATISTICS ( "STATISTICS"),
    CREATE ( "CREATE"),
    LIKE ( "LIKE"),
    SYSTEM_USER ( "SYSTEM_USER"),
    CROSS ( "CROSS"),
    LINENO ( "LINENO"),
    TABLE ( "TABLE"),
    CURRENT ( "CURRENT"),
    LOAD ( "LOAD"),
    TABLESAMPLE ( "TABLESAMPLE"),
    CURRENT_DATE ( "CURRENT_DATE"),
    MERGE ( "MERGE"),
    TEXTSIZE ( "TEXTSIZE"),
    CURRENT_TIME ( "CURRENT_TIME"),
    NATIONAL ( "NATIONAL"),
    THEN ( "THEN"),
    CURRENT_TIMESTAMP ( "CURRENT_TIMESTAMP"),
    NOCHECK ( "NOCHECK"),
    TO ( "TO"),
    CURRENT_USER ( "CURRENT_USER"),
    NONCLUSTERED ( "NONCLUSTERED"),
    TOP ( "TOP"),
    CURSOR ( "CURSOR"),
    NOT ( "NOT"),
    TRAN ( "TRAN"),
    DATABASE ( "DATABASE"),
    NULL ( "NULL"),
    TRANSACTION ( "TRANSACTION"),
    DBCC ( "DBCC"),
    NULLIF ( "NULLIF"),
    TRIGGER ( "TRIGGER"),
    DEALLOCATE ( "DEALLOCATE"),
//    OF ( "OF"),
    TRUNCATE ( "TRUNCATE"),
    DECLARE ( "DECLARE"),
    OFF ( "OFF"),
    TRY_CONVERT ( "TRY_CONVERT"),
    DEFAULT ( "DEFAULT"),
    OFFSETS ( "OFFSETS"),
    TSEQUAL ( "TSEQUAL"),
    DELETE ( "DELETE"),
    ON ( "ON"),
    UNION ( "UNION"),
    DENY ( "DENY"),
    OPEN ( "OPEN"),
    UNIQUE ( "UNIQUE"),
    DESC ( "DESC"),
    OPENDATASOURCE ( "OPENDATASOURCE"),
    UNPIVOT ( "UNPIVOT"),
    DISK ( "DISK"),
    OPENQUERY ( "OPENQUERY"),
    UPDATE ( "UPDATE"),
    DISTINCT ( "DISTINCT"),
    OPENROWSET ( "OPENROWSET"),
    UPDATETEXT ( "UPDATETEXT"),
    DISTRIBUTED ( "DISTRIBUTED"),
    OPENXML ( "OPENXML"),
    USE ( "USE"),
    DOUBLE ( "DOUBLE"),
    OPTION ( "OPTION"),
    USER ( "USER"),
    DROP ( "DROP"),
    OR ( "OR"),
    VALUES ( "VALUES"),
    DUMP ( "DUMP"),
    ORDER ( "ORDER"),
    VARYING ( "VARYING"),
    ELSE ( "ELSE"),
    OUTER ( "OUTER"),
    VIEW ( "VIEW"),
    END ( "END"),
    OVER ( "OVER"),
    WAITFOR ( "WAITFOR"),
    ERRLVL ( "ERRLVL"),
    PERCENT ( "PERCENT"),
    WHEN ( "WHEN"),
    ESCAPE ( "ESCAPE"),
    PIVOT ( "PIVOT"),
    WHERE ( "WHERE"),
    EXCEPT ( "EXCEPT"),
    PLAN ( "PLAN"),
    WHILE ( "WHILE"),
    EXEC ( "EXEC"),
    PRECISION ( "PRECISION"),
    WITH ( "WITH"),
    EXECUTE ( "EXECUTE"),
    PRIMARY ( "PRIMARY"),
    WITHIN ( "WITHIN"),
    EXISTS ( "EXISTS"),
    PRINT ( "PRINT"),
    WRITETEXT ( "WRITETEXT"),
    EXIT ( "EXIT"),
    PRO ( "PRO");

    private String abbreviation;
    private List<String> synonyms;

    public final String tag = Column.class.getSimpleName();

    static {
        List<SqlEntity> sqlWords = Arrays.stream(SqlKeywords.values()).map(v -> new SqlEntity(v.abbreviation, SqlKeywords.class.getSimpleName(),v)).collect(Collectors.toList());
        SqlEntityCache.put(sqlWords);
    }

    SqlKeywords(String abbreviation) {
        this.abbreviation = abbreviation;
//        this.synonyms = PythonService.getWordSynonyms(abbreviation);
    }



//    @Override
    public List<String> getSynonyms() {
        return synonyms;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }


//    @Override
    public String getTag() {
        return SqlKeywords.class.getSimpleName();
    }
}
