package app.base.model.sql;

import app.base.model.TagDetail;
import lombok.Data;

/**
 * @author shraddha.sahu on 27/09/18
 * @project NaQuL
 */

@Data
public class Value implements TagDetail{
    String name;
    final String tag = Value.class.getSimpleName();
    String table;

    public Value(String name, String table) {
        this.name = name;
        this.table = table;
    }
}
