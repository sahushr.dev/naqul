package app.base.model.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum KeyWordMapping {

    GROUP("GROUP BY", "FOR EACH" , "FOR ALL" , "GROUPED" ,"GROUPED IN", "GROUPED BY" , "EACH"),
    ORDER("ORDER BY", "ORDERED", "ORDERED BY", "SORTED", "SORT BY", "SORTED BY"),
    COUNT("NUMBER OF", "HOW MANY", "HOW MUCH"),
    AVG("AVERGAE", "MEAN","MEDIAN"),
    MIN("MINIMUM", "LEAST", "BOTTOM", "NADIR", "SMALLEST"),
    MAX("MAXIMUM","GREATEST", "LARGEST" , "TOP"),
    SUM("SUMMATION","TOTAL", "CUMULATIVE", "AGGREGATE");

    private Set<String> synonyms;

    private final static Map<String,KeyWordMapping> lookup = new HashMap<>();

    KeyWordMapping(String... s) {
        synonyms  = Stream.of(s).collect(Collectors.toSet());
    }

    static {
        Arrays.stream(KeyWordMapping.values()).forEach(keyWordMapping -> {
            keyWordMapping.synonyms.forEach(s -> lookup.put(s,keyWordMapping));
        });
    }

    public static Set<String> getProbableList(){
        return lookup.keySet();
    }

    public static String getKeyWord(String s){
        return lookup.get(s.toUpperCase()).name();
    }

}
