package app.base.model.sql;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */
public enum SqlOperators implements SqlKnownWords {
    ADD("+"),
    SUBTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/"),
    MODULO("%"),

    BITWISE_AND("&"),
    BITWISE_OR("|"),
    BITWISE_XOR("^"),

    EQUAL_TO("="),
    NOT_EQUAL_TO("!="),
    GREATER_THAN(">"),
    LESS_THAN("<"),
    GREATER_THAN_EQUAL_TO(">="),
    LESS_THAN_EQUAL_TO("<="),

    ALL("ALL"),
    ANY("ANY"),
    BETWEEN("BETWEEN"),
    EXISTS("EXISTS"),
    IN("IN"),
    LIKE("LIKE"),
    NOT("NOT"),
    OR("OR"),
    SOME("SOME");

    private String abbreviation;
//    private List<String> synonyms;


    static {
        List<SqlEntity> sqlWords = Arrays.asList(SqlOperators.values()).stream().map(v -> new SqlEntity(v.abbreviation, SqlOperators.class.getSimpleName(),v)).collect(Collectors.toList());
        SqlEntityCache.put(sqlWords);
    }

    SqlOperators(String abbreviation) {
        this.abbreviation = abbreviation;
//        this.synonyms = PythonService.getWordSynonyms(abbreviation);
    }


//    public List<String> getSynonyms() {
//        return synonyms;
//    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public String getTag() {
        return SqlOperators.class.getSimpleName();
    }
}
