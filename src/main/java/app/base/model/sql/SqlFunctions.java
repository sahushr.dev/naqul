package app.base.model.sql;

import app.services.PythonService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */
public enum SqlFunctions implements SqlKnownWords{
    ASCII("ASCII"),
    CHAR_LENGTH("CHAR_LENGTH"),
    CHARACTER_LENGTH("CHARACTER_LENGTH"),
    CONCAT("CONCAT"),
    CONCAT_WS("CONCAT_WS"),
    FIELD("FIELD"),
    FIND_IN_SET("FIND_IN_SET"),
    FORMAT("FORMAT"),
    INSERT("INSERT"),
    INSTR("INSTR"),
    LCASE("LCASE"),
    LEFT("LEFT"),
    LENGTH("LENGTH"),
    LOCATE("LOCATE"),
    LOWER("LOWER"),
    LPAD("LPAD"),
    LTRIM("LTRIM"),
    MID("MID"),
    POSITION("POSITION"),
    REPEAT("REPEAT"),
    REPLACE("REPLACE"),
    REVERSE("REVERSE"),
    RIGHT("RIGHT"),
    RPAD("RPAD"),
    RTRIM("RTRIM"),
    SPACE("SPACE"),
    STRCMP("STRCMP"),
    SUBSTR("SUBSTR"),
    SUBSTRING("SUBSTRING"),
    SUBSTRING_INDEX("SUBSTRING_INDEX"),
    TRIM("TRIM"),
    UCASE("UCASE"),
    UPPER("UPPER"),
    ABS("ABS"),
    ACOS("ACOS"),
    ASIN("ASIN"),
    ATAN("ATAN"),
    ATAN2("ATAN2"),
    AVG("AVG"),
    CEIL("CEIL"),
    CEILING("CEILING"),
    COS("COS"),
    COT("COT"),
    COUNT("COUNT"),
    DEGREES("DEGREES"),
    DIV("DIV"),
    EXP("EXP"),
    FLOOR("FLOOR"),
    GREATEST("GREATEST"),
    LEAST("LEAST"),
    LN("LN"),
    LOG("LOG"),
    LOG10("LOG10"),
    LOG2("LOG2"),
    MAX("MAX"),
    MIN("MIN"),
    MOD("MOD"),
    PI("PI"),
    POW("POW"),
    POWER("POWER"),
    RADIANS("RADIANS"),
    RAND("RAND"),
    ROUND("ROUND"),
    SIGN("SIGN"),
    SIN("SIN"),
    SQRT("SQRT"),
    SUM("SUM"),
    TAN("TAN"),
    TRUNCATE("TRUNCATE"),
    ADDDATE("ADDDATE"),
    ADDTIME("ADDTIME"),
    CURDATE("CURDATE"),
    CURRENT_DATE("CURRENT_DATE"),
    CURRENT_TIME("CURRENT_TIME"),
    CURRENT_TIMESTAMP("CURRENT_TIMESTAMP"),
    CURTIME("CURTIME"),
    DATE("DATE"),
    DATEDIFF("DATEDIFF"),
    DATE_ADD("DATE_ADD"),
    DATE_FORMAT("DATE_FORMAT"),
    DATE_SUB("DATE_SUB"),
    DAY("DAY"),
    DAYNAME("DAYNAME"),
    DAYOFMONTH("DAYOFMONTH"),
    DAYOFWEEK("DAYOFWEEK"),
    DAYOFYEAR("DAYOFYEAR"),
    EXTRACT("EXTRACT"),
    FROM_DAYS("FROM_DAYS"),
    HOUR("HOUR"),
    LAST_DAY("LAST_DAY"),
    LOCALTIME("LOCALTIME"),
    LOCALTIMESTAMP("LOCALTIMESTAMP"),
    MAKEDATE("MAKEDATE"),
    MAKETIME("MAKETIME"),
    MICROSECOND("MICROSECOND"),
    MINUTE("MINUTE"),
    MONTH("MONTH"),
    MONTHNAME("MONTHNAME"),
    NOW("NOW"),
    PERIOD_ADD("PERIOD_ADD"),
    PERIOD_DIFF("PERIOD_DIFF"),
    QUARTER("QUARTER"),
    SECOND("SECOND"),
    SEC_TO_TIME("SEC_TO_TIME"),
    STR_TO_DATE("STR_TO_DATE"),
    SUBDATE("SUBDATE"),
    SUBTIME("SUBTIME"),
    SYSDATE("SYSDATE"),
    TIME("TIME"),
    TIME_FORMAT("TIME_FORMAT"),
    TIME_TO_SEC("TIME_TO_SEC"),
    TIMEDIFF("TIMEDIFF"),
    TIMESTAMP("TIMESTAMP"),
    TO_DAYS("TO_DAYS"),
    WEEK("WEEK"),
    WEEKDAY("WEEKDAY"),
    WEEKOFYEAR("WEEKOFYEAR"),
    YEAR("YEAR"),
    YEARWEEK("YEARWEEK"),
    BIN("BIN"),
    BINARY("BINARY"),
    CASE("CASE"),
    CAST("CAST"),
    COALESCE("COALESCE"),
    CONNECTION_ID("CONNECTION_ID"),
    CONV("CONV"),
    CONVERT("CONVERT"),
    CURRENT_USER("CURRENT_USER"),
    DATABASE("DATABASE"),
    IF("IF"),
    IFNULL("IFNULL"),
    ISNULL("ISNULL"),
    LAST_INSERT_ID("LAST_INSERT_ID"),
    NULLIF("NULLIF"),
    SESSION_USER("SESSION_USER"),
    SYSTEM_USER("SYSTEM_USER"),
    USER("USER"),
    VERSION("VERSION");

    private String abbreviation;
    private List<String> synonyms;

    public final String tag = SqlFunctions.class.getSimpleName();

    static {
        List<SqlEntity> sqlWords = Arrays.stream(SqlFunctions.values()).map(v -> new SqlEntity(v.abbreviation, SqlFunctions.class.getSimpleName(),v)).collect(Collectors.toList());
        SqlEntityCache.put(sqlWords);
    }

    SqlFunctions(String abbreviation) {
        this.abbreviation = abbreviation;
        this.synonyms = PythonService.getWordSynonyms(abbreviation);
    }


//    @Override
    public List<String> getSynonyms() {
        return synonyms;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }


//    @Override
    public String getTag() {
        return SqlFunctions.class.getSimpleName();
    }
}
