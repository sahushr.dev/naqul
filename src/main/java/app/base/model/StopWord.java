package app.base.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StopWord implements TagDetail {
    String name;
    ClrTypes type;
    String tag;


}
