package app.base.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author shraddha.sahu on 29/09/18
 * @project NaQuL
 */

@AllArgsConstructor
@Data
public class SynonymRequest {
    String word;
}
