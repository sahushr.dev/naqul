package app.base.model.request;

import lombok.Data;

/**
 * @author shraddha.sahu on 28/09/18
 * @project NaQuL
 */
@Data
public class Request {
    String nql;
}
