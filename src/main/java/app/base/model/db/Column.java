package app.base.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Column implements DbEntity {

    @EqualsAndHashCode.Include
    String name;

    @EqualsAndHashCode.Include
    String type;

    @EqualsAndHashCode.Exclude
    Reference reference;

    @EqualsAndHashCode.Exclude
    Reference referencedBy;

    @EqualsAndHashCode.Include
    String table;

    public String tag = Column.class.getSimpleName();

    public Column(String name, String type, String table) {
        this.name = name;
        this.type = type;
        this.table = table;
    }

    public Column(String table,String name) {
        this.name = name;
        this.table = table;
    }
}
