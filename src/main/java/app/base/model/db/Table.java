package app.base.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Table implements DbEntity {

    @EqualsAndHashCode.Include
    String name;

    @EqualsAndHashCode.Exclude
    Map<String, Column> columns;


    public final String tag = Table.class.getSimpleName();

}
