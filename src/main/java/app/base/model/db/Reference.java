package app.base.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */

@Data
@AllArgsConstructor
public class Reference {
    String tableName;
    String columnName;
}
