package app.base.model.db;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Database implements DbEntity {

    @EqualsAndHashCode.Include
    String name;
    Map<String, Table> tables;

    public final String tag = Database.class.getSimpleName();

    public Database(String name, Map<String, Table> tables) {
        this.name = name;
        this.tables = tables;
        DbEntityCache.put(tables);
    }

}
