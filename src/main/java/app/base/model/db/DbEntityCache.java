package app.base.model.db;

import app.base.model.ClrTypes;
import app.base.model.sql.Value;
import app.services.PythonService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */

public class DbEntityCache {
    private static Map<String, List<DbEntity>> dbEntityCacheByName;
    private static Map<ClrTypes, Set<Value>> dbEntityCacheByType;


    public static void put(Map<String, Table> tables) {

        dbEntityCacheByName = new HashMap<>();
        dbEntityCacheByType = new HashMap<>();
        tables.forEach((key, value) -> {
            key = key.toUpperCase();
            addToCache(key, value);
            PythonService.getWordSynonyms(key).forEach(k -> addToCache(k.toUpperCase(), value));

            value.columns.forEach((key1, value1) -> {
                key1 = key1.toUpperCase();
                addToCache(key1, value1);
                PythonService.getWordSynonyms(key1).forEach(k -> addToCache(k.toUpperCase(), value1));
            });
        });
    }

    private static void addToCache(String key, DbEntity value) {

        addToCacheByName(key, value);

        if (value instanceof Column)
            addToCacheByType(((Column) value));
    }

    private static void addToCacheByType(Column column) {
        ClrTypes key = ClrTypes.getClrType(column.type);
        if (!dbEntityCacheByType.containsKey(key)) {
            dbEntityCacheByType.put(key, new HashSet<>());
        }
        dbEntityCacheByType.get(key).add(new Value(column.name, column.table));
    }

    private static void addToCacheByName(String key, DbEntity value) {
        if (!dbEntityCacheByName.containsKey(key)) {
            dbEntityCacheByName.put(key, new ArrayList<>());
        }
        dbEntityCacheByName.get(key).add(value);
    }

    public static List<DbEntity> getByName(String key) {
//        if (dbEntityCacheByName.containsKey(key)) return dbEntityCacheByName.get(key);
        String key1 = key.toUpperCase();
        List<DbEntity> list = new ArrayList();
        dbEntityCacheByName.keySet().forEach(s -> {
            if (s.equals(key1) ||
                    Arrays.stream(s.split("_")).collect(Collectors.toSet()).contains(key1))
                list.addAll(dbEntityCacheByName.get(s));
        });
        return list;
    }

    public static Set<Value> getByType(ClrTypes type) {
        return dbEntityCacheByType.getOrDefault(type,new HashSet<>());
    }

    public static boolean contains(String key) {
        return dbEntityCacheByName.containsKey(key) || dbEntityCacheByName.keySet().stream().anyMatch(s -> Arrays.stream(s.split("_")).collect(Collectors.toSet()).contains(key));
    }


}
