package app;

import app.base.model.ClrTypes;
import app.base.model.sql.SqlFunctions;
import app.base.model.sql.SqlKeywords;
import app.base.model.sql.SqlOperators;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        initEnums();
        SpringApplication.run(App.class, args);
    }

    /**
     * static blocks within enums will not get called if they are not used throughout the project
     */
    private static void initEnums(){
        SqlFunctions abs = SqlFunctions.ABS;
        SqlOperators add = SqlOperators.ADD;
        SqlKeywords table = SqlKeywords.TABLE;
        ClrTypes types = ClrTypes.DOUBLE;
    }
}
