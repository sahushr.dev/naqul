package app.services;

import app.base.model.ClrTypes;
import app.base.model.StopWord;
import app.base.model.TagDetail;
import app.base.model.TaggedWord;
import app.base.model.db.DbEntity;
import app.base.model.db.DbEntityCache;
import app.base.model.sql.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */

@Component
public class TaggingService {

    @Autowired
    DboService dboService;

    @Autowired
    StopWordService stopWordService;

    public List<TaggedWord> getSqlTaggedWords(String nql) {
        String[] split = nql.split(" ");

        return Arrays.stream(split).filter(w -> !w.isEmpty()).map(word -> {
            String upperCase = word.toUpperCase();
            TaggedWord taggedWord = new TaggedWord();
            if (SqlEntityCache.contains(upperCase)) {
                Set<SqlEntity> sqlEntity = SqlEntityCache.get(upperCase);
                taggedWord.setWord(word);
                List<TagDetail> value = taggedWord.getValue();
                sqlEntity.forEach(v -> value.add((v)));
                for (SqlEntity sqlEntity1 :
                        sqlEntity) {
                    if (sqlEntity1.name instanceof SqlOperators ||
                            sqlEntity1.name instanceof SqlFunctions ||
                            sqlEntity1.name instanceof SqlKeywords) return taggedWord;
                }
            }
            if (DbEntityCache.contains(upperCase)) {
                List<DbEntity> dbEntity = DbEntityCache.getByName(upperCase);
                taggedWord.setWord(word);
                List<TagDetail> value = taggedWord.getValue();
                dbEntity.forEach(v -> value.add((v)));
            } else if (stopWordService.isStopWord(word.toUpperCase())) return new TaggedWord(word, new ArrayList<TagDetail>() {{
                add(new StopWord(null, null, "StopWord"));
            }});
            else { //value
                ArrayList<TagDetail> taggedDetails = new ArrayList<>();
                List<ClrTypes> type = ClrTypes.getType(word);
                type.forEach(clrTypes ->
                        taggedDetails.addAll(
                                DbEntityCache.getByType(clrTypes)
                        ));
                return new TaggedWord(word, taggedDetails);
            }
            return taggedWord;

        }).filter(taggedWord -> taggedWord.getValue() != null).collect(Collectors.toList());
    }
}
