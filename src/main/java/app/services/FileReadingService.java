package app.services;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */

@Component
public class FileReadingService {

    public String readFileToString(final String path, String ... commentBeginningChar) throws IOException {
        BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(path).toAbsolutePath());
        StringBuilder content = new StringBuilder();
        List<String> comment = new ArrayList<>();
        if(commentBeginningChar!=null) comment = Arrays.asList(commentBeginningChar);
        while(true) {
            final String line = bufferedReader.readLine();
            if(line==null) break;
            boolean flag = false;
            if(comment.size() > 0)
                   flag = comment.stream().anyMatch(c -> line.contains(c));
            if (!flag)
                content.append(line).append("\n");
        }
        return content.toString();
    }
}
