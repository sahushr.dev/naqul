package app.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class StopWordService {

    private Set<String> stopWordsCache;

    @Value("${stop.word.file.path}")
    String stopWordsFilePath;

    @Autowired
    FileReadingService fileReadingService;

    @PostConstruct
    public void populateStopWord() throws IOException {
        List<String> strings = Arrays.asList(fileReadingService.readFileToString(stopWordsFilePath).split("\n"));
        stopWordsCache = strings.stream().map(String::toUpperCase).collect(Collectors.toSet());
    }

    public boolean isStopWord(String word) {
        return stopWordsCache.contains(word);
    }
}
