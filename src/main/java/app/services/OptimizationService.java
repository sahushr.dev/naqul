package app.services;

import app.base.model.query.Join;
import app.base.model.query.Query;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author shraddha.sahu on 28/09/18
 * @project NaQuL
 */

@Component
public class OptimizationService {
    public Set<String> optimize(Set<String> validSqls, String nql) {
        if (validSqls == null || validSqls.isEmpty()) return validSqls;

        Map<Integer, Set<String>> optimizedList = new HashMap<>();
        int c = 0;
        String[] split = nql.split(" ");
        HashSet<String> nqlWords = new HashSet<>(Arrays.asList(split));

        for (String sql :
                validSqls) {
            String[] split1 = sql.split("[\t_ ,.]");
            HashSet<String> sqlWords = new HashSet<>(Arrays.asList(split1));
            sqlWords.retainAll(nqlWords);
            int size = sqlWords.size();
            if (!optimizedList.containsKey(size)) optimizedList.put(size, new HashSet<>());
            optimizedList.get(size).add(sql);
            if (size > c) {
                c = size;
            }
        }
        return optimizedList.get(c);
    }


    public Query optimizeQueryObject(Query query) {

        Set<String> groupByList = new HashSet<>();

        if (query.getGroupByList() != null) {
            for (String g : query.getGroupByList()) {
                if (g.split("\\.")[0].equals(query.getFrom())) groupByList.add(g);
            }
        }

        if (query.getJoinList() != null && !query.getJoinList().isEmpty()) {
            List<Join> joinList = query.getJoinList();
            Collections.reverse(joinList);
            List<Join> newJoinList = new ArrayList<>();
            query.getWhereList().forEach(s -> {
                String whereTable = s.split("\\.")[0];
                joinList.forEach(join -> {
                    if (join.getTable().equals(whereTable)) {
                        newJoinList.add(join);
                    }
                });
            });
            query.setJoinList(newJoinList);

            Set<String> joinTables = joinList.stream().map(join -> join.getTable()).collect(Collectors.toSet());

            if (query.getGroupByList() != null && joinTables != null) {
                for (String g : query.getGroupByList()) {
                    if (joinTables.contains(g.split("\\.")[0])) groupByList.add(g);
                }
            }


        } else if (query.getWhereList() != null && !query.getWhereList().isEmpty()) {
            for (String where : query.getWhereList()) {
                if (!where.split("\\.")[0].equals(query.getFrom())) return null;
            }
        }

        query.setGroupByList(groupByList);
        return query;
    }
}
