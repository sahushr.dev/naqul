package app.services;

import app.base.model.ClrTypes;
import app.base.model.TagDetail;
import app.base.model.TaggedWord;
import app.base.model.db.*;
import app.base.model.query.Join;
import app.base.model.query.Query;
import app.base.model.sql.*;
import org.apache.commons.text.similarity.CosineSimilarity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */
@Component
public class QueryService {


    @Autowired
    TaggingService taggingService;

    @Autowired
    OptimizationService optimizationService;

    @Autowired
    StopWordService stopWordService;

    public Set<String> parseQuery(String nql) {

        for (String key : KeyWordMapping.getProbableList()) {
            String upperNql = nql.toUpperCase();
            if (upperNql.contains(key))
                nql = upperNql.replace(key, KeyWordMapping.getKeyWord(key));
        }

        System.out.println(nql);

        List<TaggedWord> taggedWords = taggingService.getSqlTaggedWords(nql);


        final List<Set<String>> words = prepareMustHaveWordLists(taggedWords);
        final Set<Query> queries = prepareQueries(taggedWords);

        Set<String> validSqls = queries.stream().map(query -> {
            try {
                query = optimizationService.optimizeQueryObject(query);
                return buildQuery(query, taggedWords);
            } catch (NullPointerException ex) {
                System.out.println("Error in query : " + query);
                return null;
            }
        }).filter(Objects::nonNull).map(query -> {
            HashSet<String> queryWordsSet = new HashSet<>(Arrays.asList(query.split("[._ ']")));
            if (isRelevantQuery(queryWordsSet, words)) return query;
            return null;
        }).filter(query -> query != null).collect(Collectors.toSet());


        validSqls = optimizationService.optimize(validSqls, nql);
        validSqls.forEach(System.out::println);

        final String input = nql;
        List<String> soertedQueries = new ArrayList<>(validSqls);
        soertedQueries.sort((o1, o2) -> {
            double v = getCosineSimilarity(input, o1.toString()) - getCosineSimilarity(input, o2.toString());
            if (v > 0) return 1;
            if (v < 0) return -1;
            return 0;
        });

        return new HashSet<>(soertedQueries);
    }


    private static String buildQuery(Query query, List<TaggedWord> taggedWords) throws NullPointerException {

        Map<String, Set<String>> map = groupByTag(taggedWords);
        Set<String> tables = new HashSet<String>() {{
            add(query.getFrom());
        }};
        if (query.getJoinList() != null)
            query.getJoinList().forEach(join -> tables.add(join.getTable()));


        Set<String> whereSet = null;
        if (query.getWhereList() != null)
            whereSet = query.getWhereList().stream().filter(s -> tables.contains(s.split("\\.")[0])).collect(Collectors.toSet());
        Set<String> groupBySet = null;
        if (query.getGroupByList() != null)
            groupBySet = query.getGroupByList().stream().filter(s -> tables.contains(s.split("\\.")[0])).collect(Collectors.toSet());
        Set<String> selectSet = null;
        if (query.getSelectList() != null)
            selectSet = query.getSelectList().stream().filter(s -> {
                String[] split = s.split("\\.");
                return split.length > 0 && tables.contains(split[0]);
            }).collect(Collectors.toSet());

        Set<String> orderSet = null;
        if (query.getOrderList() != null)
            orderSet = query.getOrderList().stream().filter(s -> tables.contains(s.split("\\.")[0])).collect(Collectors.toSet());


        query.setWhereList(whereSet);
        query.setGroupByList(groupBySet);
        query.setSelectList(selectSet);
        query.setOrderList(orderSet);

        StringBuilder sql = new StringBuilder();
        String from = query.fromAsString();
        String where = query.whereAsString();
        String join = query.joinListAsString();
        String groupBy = query.groupByAsString();
        String having = query.havingListAsString();
        String order = query.orderAsString();

        if (map.containsKey(SqlOperators.class.getSimpleName())) {
            Set<String> sqlOperators = map.get(SqlOperators.class.getSimpleName());
            where = query.whereAsString(sqlOperators);
        }

        Set<String> words = new HashSet<>();
        if (map.containsKey(SqlKeywords.class.getSimpleName())) {
            words.addAll(map.get(SqlKeywords.class.getSimpleName()));
        }
        if (map.containsKey(SqlFunctions.class.getSimpleName())) {
            words.addAll(map.get(SqlFunctions.class.getSimpleName()));
        }
        String select = query.selectAsString(words);

        if (select == null || select.isEmpty() || from == null || from.isEmpty())
            throw new NullPointerException("SELECT or FROM list empty");


        sql.append(select).append(from);
        if (join != null && !join.isEmpty()) sql.append(join);
        if (where != null && !where.isEmpty()) sql.append(where);
        if (groupBy != null && !groupBy.isEmpty()) sql.append(groupBy);
        if (having != null && !having.isEmpty()) sql.append(having);
        if (order != null && !order.isEmpty()) sql.append(order);

        return sql.toString();
    }

    private static Map<String, Set<String>> groupByTag(List<TaggedWord> taggedWords) {
        Map<String, Set<String>> map = new HashMap<>();
        for (TaggedWord word :
                taggedWords) {
            for (TagDetail tagDetail :
                    word.getValue()) {
                if (!map.containsKey(tagDetail.getTag())) {
                    map.put(tagDetail.getTag(), new HashSet<>());
                }
                map.get(tagDetail.getTag()).add(tagDetail.getName());
            }
        }
        return map;
    }

    private Set<Query> prepareQueries(List<TaggedWord> taggedWords) {
        List<String> fromCandidates = getFromCandidates(taggedWords);
        List<Set<String>> selectCandidates = fromCandidates.stream().map(from ->
                getSelectCandidates(taggedWords, from)).collect(Collectors.toList());

        Set<String> groupByCandidates = groupByCandidates(taggedWords);
        Set<String> orderByCandidtes = orderByCandidates(taggedWords);

        List<TaggedWord> values = getValues(taggedWords);
        boolean valueOrOperatorPresent = valueOrOperatorPresent(taggedWords);

        Set<Query> queries = new HashSet<>();
        IntStream.range(0, fromCandidates.size()).forEach(i -> {
            if (valueOrOperatorPresent) {
                List<List<Join>> joins = getJoins(fromCandidates.get(i), taggedWords);
                for (List<Join> join : joins) {
                    List<Set<String>> wheres = getWheres(fromCandidates.get(i), join, values, taggedWords);
                    for (Set<String> wheres1 : wheres) {
                        queries.add(preparePossibleQuery(fromCandidates.get(i), selectCandidates.get(i), join, wheres1, groupByCandidates, orderByCandidtes));
                    }
                }
            }
            queries.add(preparePossibleQuery(fromCandidates.get(i), selectCandidates.get(i), null, null, groupByCandidates, orderByCandidtes));
        });
        return queries;
    }

    private Set<String> orderByCandidates(List<TaggedWord> taggedWords) {

        Map<String, Set<String>> tagMap = groupByTag(taggedWords);

        boolean flag = false;
        Set<String> result = new HashSet<>();
        if (tagMap.containsKey(SqlKeywords.class.getSimpleName()) &&
                tagMap.get(SqlKeywords.class.getSimpleName()).contains(SqlKeywords.ORDER.getAbbreviation())) {
            for (TaggedWord taggedWord :
                    taggedWords) {
                if (!flag) {
                    for (TagDetail tagDetail : taggedWord.getValue()) {
                        if (tagDetail.getName() != null && tagDetail.getName().equals(SqlKeywords.ORDER.getAbbreviation())) {
                            flag = true;
                        }
                    }
                } else {
                    for (TagDetail tagDetail : taggedWord.getValue()) {
                        if (tagDetail.getTag().equals(SqlKeywords.class.getSimpleName()) ||
                                tagDetail.getTag().equals(SqlOperators.class.getSimpleName()) ||
                                tagDetail.getTag().equals(SqlFunctions.class.getSimpleName()))
                            break;

                        if (tagDetail instanceof Column)
                            result.add(((Column) tagDetail).getTable() + "." + tagDetail.getName());
                    }
                }

            }
        }

        return result;
    }

    private Set<String> groupByCandidates(List<TaggedWord> taggedWords) {
        Map<String, Set<String>> tagMap = groupByTag(taggedWords);

        boolean flag = false;
        Set<String> result = new HashSet<>();
        if (tagMap.containsKey(SqlKeywords.class.getSimpleName()) &&
                tagMap.get(SqlKeywords.class.getSimpleName()).contains(SqlKeywords.GROUP.getAbbreviation())) {
            for (TaggedWord taggedWord :
                    taggedWords) {
                if (!flag) {
                    for (TagDetail tagDetail : taggedWord.getValue()) {
                        if (tagDetail.getName() != null && tagDetail.getName().equals(SqlKeywords.GROUP.getAbbreviation())) {
                            flag = true;
                        }
                    }
                } else {
                    for (TagDetail tagDetail : taggedWord.getValue()) {
                        if (tagDetail.getTag().equals(SqlKeywords.class.getSimpleName()) ||
                                tagDetail.getTag().equals(SqlOperators.class.getSimpleName()) ||
                                tagDetail.getTag().equals(SqlFunctions.class.getSimpleName()))
                            break;

                        if (tagDetail instanceof Column)
                            result.add(((Column) tagDetail).getTable() + "." + tagDetail.getName());
                    }
                }

            }
        }

        return result;
    }

    private Set<String> getWhereForNoJoin(String fromTable, Set<String> wheres1) {
        return wheres1.stream().filter(s -> s.contains(fromTable)).collect(Collectors.toSet());
    }

    private List<TaggedWord> getValues(List<TaggedWord> taggedWords) {
        List<TaggedWord> values = taggedWords.stream().filter(taggedWord ->
                taggedWord.getValue().get(0) instanceof Value && taggedWord.getValue().get(0).getTag().equals(Value.class.getSimpleName())
        ).collect(Collectors.toList());
        return values;
    }

    private boolean valueOrOperatorPresent(List<TaggedWord> taggedWords) {
        for (TaggedWord taggedWord :
                taggedWords) {
            Set<String> tags = taggedWord.getValue().stream().map(TagDetail::getTag).collect(Collectors.toSet());
            if (tags.contains(Value.class.getSimpleName()) || tags.contains(SqlOperators.class.getSimpleName()))
                return true;
        }
        return false;
    }

    private List<Set<String>> getWheres(String fromTable, List<Join> joins, List<TaggedWord> values, List<TaggedWord> taggedWords) {

        Set<Column> columns = getColumnUsed(fromTable, joins, taggedWords).stream()
                .map(column -> new Column(column.getTable(), column.getName())).collect(Collectors.toSet());

        List<TaggedWord> shortenedTaggedValues = values.stream().map(value -> {
            List<TagDetail> modifiedTageedDetails = value.getValue().stream().filter(tagDetail -> {
                Value detail = (Value) tagDetail;
                return columns.contains(new Column(detail.getTable(), detail.getName()));
            }).collect(Collectors.toList());
            return new TaggedWord(value.getWord(), modifiedTageedDetails);
        }).collect(Collectors.toList());

        if (!shortenedTaggedValues.isEmpty()) {
            List<Set<String>> collect = shortenedTaggedValues.get(0).getValue().stream().map(tagDetail -> {
                if (tagDetail instanceof Value) {
                    String table = ((Value) tagDetail).getTable();
                    String name = tagDetail.getName();
                    if (columns.contains(new Column(table, tagDetail.getName()))) {
                        return new HashSet<String>() {{
                            String word = shortenedTaggedValues.get(0).getWord();
                            if (ClrTypes.getType(word).contains(ClrTypes.STRING))
                                add(table + "." + name + " = '" + word + "' ");
                            else add(table + "." + name + " = " + word + " ");

                        }};
                    }
                }
                return null;
            }).filter(Objects::nonNull).collect(Collectors.toList());
            return collect;
        }

        return new ArrayList<>();
    }

    private Set<Column> getColumnUsed(String fromTable, List<Join> joins, List<TaggedWord> taggedWords) {

        int i = getIndexOfFirstDbEntity(taggedWords);

        Set<Column> columns = new HashSet<>();

        Set<String> tablesToBeUsed = joins.stream().map(Join::getTable).collect(Collectors.toSet());
        tablesToBeUsed.add(fromTable);

        if (i == -1) {
            for (String table : tablesToBeUsed) {
                for (DbEntity dbEntity : DbEntityCache.getByName(table)) {
                    if (dbEntity instanceof Table) {
                        columns.addAll(((Table) dbEntity).getColumns().values());
                    } else if (dbEntity instanceof Column) {
                        columns.add((Column) dbEntity);
                    }
                }
            }

        } else {
            List<TagDetail> value = taggedWords.get(i).getValue();
            for (TagDetail tagDetail : value) {
                if (tagDetail instanceof Column && tablesToBeUsed.contains(((Column) tagDetail).getTable()))
                    columns.add((Column) tagDetail);
                if (tagDetail instanceof Table && tablesToBeUsed.contains(tagDetail.getName())) {
                    HashSet<Column> columns1 = new HashSet<>(((Table) tagDetail).getColumns().values());
                    columns.addAll(columns1);
                }
            }
        }

        return columns;
    }

    private int getIndexOfFirstDbEntity(List<TaggedWord> taggedWords) {
        int i = -1;
        for (int j = 0; j < taggedWords.size(); j++) {
            String tag = taggedWords.get(j).getValue().get(0).getTag();
            if (tag.equals("Table") || tag.equals("Column")) i = j;
            if (tag.equals("Value")) break;
        }
        return i;
    }


    private List<List<Join>> getJoins(String fromTable, List<TaggedWord> taggedWords) {
        List<List<Join>> joins = new ArrayList<>();

        Table rootTable = null;
        List<DbEntity> dbEntities = DbEntityCache.getByName(fromTable);
        for (DbEntity dbEntity :
                dbEntities) {
            if (dbEntity instanceof Table && dbEntity.getName().equals(fromTable)) {
                rootTable = (Table) dbEntity;
                break;
            }

        }

        if (rootTable != null) {
            rootTable.getColumns().values().forEach(column -> {
                Reference reference = column.getReference();
                Reference referencedBy = column.getReferencedBy();
                if (reference != null && !reference.getTableName().equals(fromTable)) {
                    List<Join> joins1 = new ArrayList<>();
                    joins1.add(new Join(reference.getTableName(), getOnExpression(column, reference)));
                    joins.add(joins1);
                }
                if (referencedBy != null && !referencedBy.getTableName().equals(fromTable)) {
                    List<Join> joins1 = new ArrayList<>();
                    joins1.add(new Join(referencedBy.getTableName(), getOnExpression(column, referencedBy)));
                    joins.add(joins1);

                }
            });
        }


        return joins;
    }

    private String getOnExpression(Column column, Reference reference) {
        return column.getTable() + "." + column.getName() + "=" + reference.getTableName() + "." + reference.getColumnName();
    }


    private Query preparePossibleQuery(String from, Set<String> selects, List<Join> joins, Set<String> wheres, Set<String> groupBy, Set<String> orderByCandidtes) {
        Query query = new Query();
        query.setSelectList(selects);
        query.setFrom(from);
        query.setJoinList(joins);
        query.setGroupByList(groupBy);
        query.setOrderList(orderByCandidtes);
        query.setWhereList(wheres);
        return query;
    }

    private Set<String> getSelectCandidates(List<TaggedWord> taggedWords, String from) {

        return taggedWords.stream().flatMap(taggedWord -> {
            return taggedWord.getValue().stream()
                    .flatMap(tagDetail -> {
                        if (tagDetail instanceof Column && ((Column) tagDetail).getTable().equalsIgnoreCase(from)) {
                            return Stream.of(tagDetail.getName());
                        }
                        return Stream.of();
                    });
        }).collect(Collectors.toSet());
    }


    private List<String> getFromCandidates(List<TaggedWord> taggedWords) {
        TaggedWord firstRelevantWord = null;
        for (TaggedWord taggedWord :
                taggedWords) {
            String tag = taggedWord.getValue().get(0).getTag();
            if (tag.equalsIgnoreCase("Table") || tag.equalsIgnoreCase("Column")) {
                firstRelevantWord = taggedWord;
                break;
            }
        }
        if (firstRelevantWord == null) return new ArrayList<>();

        return firstRelevantWord.getValue().stream().map(tagDetail -> {
            if (tagDetail instanceof Table) {
                return tagDetail.getName();
            }

            return ((Column) tagDetail).getTable();

        }).collect(Collectors.toList());

    }

    private List<Set<String>> prepareMustHaveWordLists(List<TaggedWord> taggedWords) {
        return taggedWords.stream().map(taggedWord ->
                taggedWord.getValue().stream()
                        .map(TagDetail::getName)
                        .collect(Collectors.toSet()))
                .collect(Collectors.toList());
    }


    private Boolean isRelevantQuery(Set<String> sql, List<Set<String>> words) {
//        return words.stream().map(word -> {
//            word.removeIf(Objects::isNull);
//            word = word.stream().flatMap(s -> Stream.of(s.split("[._ ']"))).filter(s -> !stopWordService.isStopWord(s)).collect(Collectors.toSet());
//            if (word.size() > 0) {
//                word.retainAll(sql);
//                return word.size() != 0;
//            }
//            return true;
//        }).reduce(true, Boolean::logicalAnd);
        return true;
    }



    public double getCosineSimilarity(String s1, String s2){
        CosineSimilarity dist = new CosineSimilarity();

        Map<CharSequence, Integer> leftVector =
                Arrays.stream(s1.split(""))
                        .collect(Collectors.toMap(c -> c, c -> 1, Integer::sum));
        Map<CharSequence, Integer> rightVector =
                Arrays.stream(s2.split(""))
                        .collect(Collectors.toMap(c -> c, c -> 1, Integer::sum));

        return 1 - dist.cosineSimilarity(leftVector, rightVector);
    }

}
