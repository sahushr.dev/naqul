package app.services;

import app.base.model.request.SynonymRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author shraddha.sahu on 25/09/18
 * @project NaQuL
 */

public class PythonService {

    static String baseUrl = "http://192.168.1.6:8081/sys";

    public static List<String> getWordSynonyms(String word) {
        return post(word);
    }


    public static List<String> post(String word) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List> exchange = restTemplate.exchange(baseUrl, HttpMethod.POST, new HttpEntity<>(new SynonymRequest(word)), List.class);
        if (!exchange.getStatusCode().is2xxSuccessful()) return Collections.emptyList();
        return exchange.getBody();
    }

}
