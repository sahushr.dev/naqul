package app.services;

import app.base.model.db.Column;
import app.base.model.db.Database;
import app.base.model.db.Reference;
import app.base.model.db.Table;
import app.base.model.sql.SqlKeywords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author shraddha.sahu on 24/09/18
 * @project NaQuL
 */

@Component
public class DboService {

    @Value("${schema.structure.file}")
    private String schemaSqlFilePath;

    Database database;

    @Autowired
    FileReadingService fileReadingService;

    @PostConstruct
    public void prepareDatabaseObject() throws IOException {
        String sqls = fileReadingService.readFileToString(schemaSqlFilePath, "/*", "--");
        Map<String, Table> tables = Arrays.stream(sqls.split(";")).map(sql -> {
            if (sql.contains(SqlKeywords.CREATE.getAbbreviation())) return createTableObject(sql);
            return null;
        }).filter(Objects::nonNull).collect(Collectors.toMap(Table::getName, e -> e));

        tables.forEach((tableName, table) -> {
            table.getColumns().forEach((columnName, column) -> {
                Reference reference = column.getReference();
                if (reference != null) {
                    Reference referencedBy = new Reference(tableName, columnName);
                    tables.get(reference.getTableName()).getColumns().get(reference.getColumnName()).setReferencedBy(referencedBy);
                }
            });
        });

        database = new Database("", tables);

    }


    private Table createTableObject(final String sql) {
        String[] split = sql.trim().replaceAll(" +", " ").replace(" `", "`").split("\n");
        Pattern p = Pattern.compile(".*`(.*)`.*");
        String tableName = "";
        Map<String, Column> columns = new HashMap<>();
        for (String s : split) {
            if (s.contains(SqlKeywords.TABLE.getAbbreviation())) tableName = getEntityName(p, s);
            else if (!s.contains(SqlKeywords.KEY.getAbbreviation()) && s.contains("`")) {
                Column column = prepareColumn(p, s, tableName);
                if (column != null)
                    columns.put(column.getName(), column);
            } else if (s.contains(SqlKeywords.FOREIGN.getAbbreviation())) {
                String foreignKeyDetails = s.split(SqlKeywords.FOREIGN + " " + SqlKeywords.KEY)[1];
                String[] splits = foreignKeyDetails.split(SqlKeywords.REFERENCES.getAbbreviation());
                String currentcolumName = getEntityName(p, splits[0]);
                String[] referenceNames = splits[1].split("[()]");
                String referencedTableName = getEntityName(p, referenceNames[0]);
                String referenceColumnName = getEntityName(p, referenceNames[1]);
                Reference reference = new Reference(referencedTableName, referenceColumnName);
                tableName = "";
                if (columns.containsKey(currentcolumName)) {
                    Column column = columns.get(currentcolumName);
                    column.setReference(reference);
                    tableName = column.getTable();
                }

            }
        }
        return new Table(tableName, columns.values().stream().collect(Collectors.toMap(Column::getName, e -> e)));
    }

    private String getEntityName(Pattern pattern, String line) {
        Matcher m = pattern.matcher(line);
        if (m.find()) return m.group(1);
        return null;
    }

    private Column prepareColumn(Pattern p, String line, String tableName) {
        final String columnName = getEntityName(p, line);
        if (columnName != null) {
            int indexOfType = line.indexOf(columnName) + columnName.length() + 2;
            String type = null;
            if (indexOfType < line.length()) {
                String substring = line.substring(indexOfType);
                if (substring.contains(" "))
                    type = substring.substring(0, substring.indexOf(" "));
                else if (substring.contains(",")) type = substring.substring(0, substring.indexOf(","));
            } else type = null;

            return new Column(columnName, type, tableName);
        }
        return null;
    }
}
